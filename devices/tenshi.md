---
codename: 'tenshi'
name: 'Bq Aquaris U Plus'
comment: 'wip'
icon: 'phone'
noinstall: true
maturity: .6
---

I experienced to install Ubuntu Touch on BQ U Plus (tenshi) either for 2GB of RAM/16GB of ROM and 3GB/32GB configurations. You have to take a number of preparatory steps:

1. Ensure you have upgraded the stock firmware at least to Android 7.1.
2. [Enable developer options](https://www.howtogeek.com/129728/how-to-access-the-developer-options-menu-and-enable-usb-debugging-on-android-4.2/) for your device,
3. In the developer options, [enable ADB debugging](https://wiki.lineageos.org/adb_fastboot_guide.html) and OEM unlocking,
4. Install ADB on your computer, reboot your device to bootloader
5. You can unlock your device by using `fastboot`: `fastboot flash oem unlock`

### Device specifications
|    Component | Details                                                               |
|-------------:|-----------------------------------------------------------------------|
|      Chipset | Qualcomm MSM8937 Snapdragon 430                                       |
|          CPU | Octa-core Cortex-A53 1.4 GHz                                          |
| Architecture | armhf                                                                 |
|          GPU | Adreno 505                                                            |
|      Display | 127 mm (5.0 in) 1280x720 (~294 PPI) IPS LCD, 16M colors               |
|      Storage | 16/32 GB                                                              |
| Shipped Android Version | 7.0                                                        |
|       Memory | 2/3 GB                                                                |
|      Cameras | 16 MP, Dual LED (flash), 5 MP (No flash)                              |
|      Battery | Non-removable Li-Ion 3.300 mAh                                        |
|   Dimensions | 143 mm (5.64 in) (h) 72.2 mm (2.84 in) (w) 8.7 mm (0.34 in)           |
|      MicroSD | microSD, microSDHC, microSDXC up to 32MB                              |
| Release Date | 2015                                                                  |
|          SIM | nano (dual-sim)                                                       |

### Maintainer(s)

- [guf](https://forums.ubports.com/user/guf)

#### Forum topic

- https://forums.ubports.com/topic/2941/porting-ubuntu-touch-ubports-to-tenshi-bq-aquaris-u-plus
 
#### Kernel

- [http://github.com/Halium/android_kernel_bq_msm8937](http://github.com/Halium/android_kernel_bq_msm8937)

#### Device

- [http://github.com/Halium/android_device_bq_tenshi](http://github.com/Halium/android_device_bq_tenshi)


